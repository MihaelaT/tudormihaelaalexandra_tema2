const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

// metoda PUT pentru update-ul de produse in functie de id
app.put('/update',(req,res) => {
   const id = req.params.id;
   let updated = false;
   products.forEach((product) =>{
      if(product.id == id){
         updated = true;
         product.productName = req.body.productName;
         product.price = req.body.price;
      } 
   });
   if(updated){
    res.status(200).send(`Produsul cu id-ul ${id} a fost modificat cu succes!`);    
   } else {
       res.status(404).send(`Produsul cu id-ul ${id} nu a fost gasit`);
   }
});

//metoda DELETE pntru stergerea de produse in functie de denumire
app.delete('/delete',(req,res) => {
    const name = req.body.productName;
    let index = -1;
    let updated = false;
    products.forEach((product)=>{
        index += 1;
        if(product.productName === name){
            products.splice(index,1);
            updated=true;
        }
    });
     if(updated){
            res.status(200).send(`Produsul cu numele ${name} a fost sters cu succes!`);
        }
        else{
            res.status(400).send(`Produsul cu numele ${name} nu a fost gasit !!!`);
        }
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});